
import { ComponentServer } from 'framework222';

export interface IMainProps {}
export default class Main extends ComponentServer<IMainProps, any> {

    counter(data) {
        this.emit('main:counter', { counter: data.counter+1 })
    }
}