//#COMMON
import g from './global.client'
import { ComponentClient } from 'framework222-client/component';
//#COMMON END 

import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Grid, Row, Col, Label, Button, Navbar, Nav, NavItem, NavDropdown, MenuItem, FormGroup, FormControl, ControlLabel, Glyphicon } from 'react-bootstrap';


export interface IMainProps {}
export class Main extends ComponentClient<IMainProps, any> {

    state = {
        counter: 0
    }

    constructor (props) {
        super(props)
        g.components["main"] = this
        this.count = this.count.bind(this)
    }

    counter(msg){
        this.state.counter = msg.counter
        this.setState(this.state)
    }

    count(){
        this.emit('main:counter', { counter: this.state.counter })
    }

    render () {
        return (
            <div>
                <div>
                    Counter: {this.state.counter}
                </div>
                <Button bsStyle="success" onClick={this.count}><Glyphicon glyph="floppy-add" /> Count</Button>
            </div>
        );
    }
}

ReactDOM.render(React.createElement(Main), document.getElementById('root'))
