console.log("helloworld started...");
//EXPRESS ROUTES
var express = require('express');
var app = express();
var fs = require('fs');
app.use(express.static('./dist/client'));
app.get("/", function (req, res) {
    res.send("ok");
    res.end();
});
//WEB SERVER
var http = require('http');
//var https = require('https')
var server = http.createServer(app);
//var server = https.createServer(options, app).listen(443)
//WEB SOCKET
var ioserver = require('socket.io')(server);
ioserver.on('connection', function (socket) {
    console.log("helloworld: new client socket connected");
    socket.use((params, next) => {
        var message = params[0];
        var splittedMessage = message.split(':');
        var component = splittedMessage[0];
        var method = splittedMessage[1];
        var data = params[1];
        var componentModule = require('./' + component + '.server');
        var componentInstance = new componentModule.default();
        componentInstance["socket"] = socket;
        componentInstance[method](data);
        return next();
    });
    socket.on('disconnect', function () { });
});
var host = process.env.IP || "127.0.0.1";
var port = parseInt(process.env.PORT) || 8080;
server.listen(port, host, function () {
    console.log('helloworld started on %s:%d ...', host, port);
});
