"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const framework222_1 = require("framework222");
class Main extends framework222_1.ComponentServer {
    counter(data) {
        this.emit('main:counter', { counter: data.counter + 1 });
    }
}
exports.default = Main;
