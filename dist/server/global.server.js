"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Global {
    constructor() {
        if (Global._instance) {
            throw new Error("The Global is a singleton class and cannot be created!");
        }
        Global._instance = this;
        this.components = {};
    }
    static getInstance() {
        return Global._instance;
    }
}
Global._instance = new Global();
exports.Global = Global;
var g = Global.getInstance();
exports.default = g;
