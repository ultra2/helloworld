var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define("global.client", ["require", "exports", "socket.io-client"], function (require, exports, io) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Global = (function () {
        function Global() {
            if (Global._instance) {
                throw new Error("The Global is a singleton class and cannot be created!");
            }
            Global._instance = this;
            this.components = {};
            this.socket = io.connect();
            var onevent = this.socket.onevent;
            this.socket.onevent = function (packet) {
                var args = packet.data || [];
                onevent.call(this, packet);
                packet.data = ["*"].concat(args);
                onevent.call(this, packet);
            };
            this.socket.on("*", function (message, data) {
                var splittedMessage = message.split(':');
                var component = splittedMessage[0];
                var method = splittedMessage[1];
                var componentInstance = this.components[component];
                if (componentInstance != null) {
                    componentInstance[method](data);
                }
            }.bind(this));
        }
        Global.getInstance = function () {
            return Global._instance;
        };
        Global._instance = new Global();
        return Global;
    }());
    exports.Global = Global;
    var g = Global.getInstance();
    exports.default = g;
});
define("main.client", ["require", "exports", "global.client", "node_modules/framework222-client/component", "react", "react-dom", "react-bootstrap"], function (require, exports, global_client_1, component_1, React, ReactDOM, react_bootstrap_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Main = (function (_super) {
        __extends(Main, _super);
        function Main(props) {
            var _this = _super.call(this, props) || this;
            _this.state = {
                counter: 0
            };
            global_client_1.default.components["main"] = _this;
            _this.count = _this.count.bind(_this);
            return _this;
        }
        Main.prototype.counter = function (msg) {
            this.state.counter = msg.counter;
            this.setState(this.state);
        };
        Main.prototype.count = function () {
            this.emit('main:counter', { counter: this.state.counter });
        };
        Main.prototype.render = function () {
            return (React.createElement("div", null,
                React.createElement("div", null,
                    "Counter: ",
                    this.state.counter),
                React.createElement(react_bootstrap_1.Button, { bsStyle: "success", onClick: this.count },
                    React.createElement(react_bootstrap_1.Glyphicon, { glyph: "floppy-add" }),
                    " Count")));
        };
        return Main;
    }(component_1.ComponentClient));
    exports.Main = Main;
    ReactDOM.render(React.createElement(Main), document.getElementById('root'));
});
