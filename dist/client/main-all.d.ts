/// <reference types="react" />
declare module "global.client" {
    export class Global {
        private static _instance;
        components: any;
        socket: any;
        constructor();
        static getInstance(): Global;
    }
    var g: Global;
    export default g;
}
declare module "main.client" {
    import { ComponentClient } from "node_modules/framework222-client/component";
    export interface IMainProps {
    }
    export class Main extends ComponentClient<IMainProps, any> {
        state: {
            counter: number;
        };
        constructor(props: any);
        counter(msg: any): void;
        count(): void;
        render(): JSX.Element;
    }
}
