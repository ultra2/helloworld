import * as io from 'socket.io-client'

export class Global {

    private static _instance: Global = new Global();
    public components: any
    public socket: any

    constructor() {
        if(Global._instance) {
            throw new Error("The Global is a singleton class and cannot be created!");
        }

        Global._instance = this;

        this.components = {}
        this.socket = io.connect()
        
        var onevent = this.socket.onevent
        this.socket.onevent = function (packet) {
            var args = packet.data || []
            onevent.call(this, packet)    // original call
            packet.data = ["*"].concat(args)
            onevent.call(this, packet)    // additional call to catch-all
        }
        
        this.socket.on("*",function(message, data) {
            var splittedMessage = message.split(':')
            var component = splittedMessage[0]
            var method = splittedMessage[1]

            var componentInstance = this.components[component]
            if (componentInstance != null){
                componentInstance[method](data)
            }
        }.bind(this))
    }

    public static getInstance() : Global {
        return Global._instance;
    }
}

var g = Global.getInstance()
export default g