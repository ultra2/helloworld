
export class Global {

    private static _instance: Global = new Global();
    public components: any

    constructor() {
        if(Global._instance) {
            throw new Error("The Global is a singleton class and cannot be created!");
        }

        Global._instance = this;

        this.components = {}
    }

    public static getInstance() : Global {
        return Global._instance;
    }
}

var g = Global.getInstance()
export default g